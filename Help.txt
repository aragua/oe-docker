###############################
#    Setup GEH0003 project    #
###############################

0) get sources
--------------

git clone https://gitlab.collabora.com/aragua/oe-docker.git
git clone git://git.openembedded.org/openembedded-core oe-core
cd oe-core
git clone git://git.openembedded.org/bitbake bitbake
git clone git://git.openembedded.org/meta-openembedded

# Collabora stuff
git clone https://gitlab.collabora.com/aragua/meta-collabora.git

## BSP:
# rpi

git clone git://git.yoctoproject.org/meta-raspberrypi

# lepotato

git clone git://github.com/superna9999/meta-meson.git

# riotboard

git clone git://github.com/Freescale/meta-freescale.git
git clone git://github.com/Freescale/meta-freescale-3rdparty.git

1) Create a container
-----------------------------

docker build -t oe .

2) Start container
------------------

docker run -v $(pwd)/oe-core:/home/oeuser/oe-core --security-opt label=disable -ti oe

3) source env
-------------

3.1) Creation
------------

TEMPLATECONF=meta-collabora/userconf/rpi3/ source oe-init-build-env build-rpi3

3.2) Use
-------

source oe-init-build-env build-rpi3

4) Build
--------

4.1) Images
----------

bitbake collabora-image-weston
